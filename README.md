# EZXR Object Detection

EZXR Object Detection 易现物体检测算法

## 对应版本

| Unity Version | EZXR Object Detection |
| ------------- | ----------- |
| 2022.3        | 0.1.1       |

## Release Note

### V0.1.0

1. 提供iOS/android EZXR SObject Detection 易现物体检测算法 算法库
2. 提供C#调用算法接口ObjectDetectionController类



## API 说明

