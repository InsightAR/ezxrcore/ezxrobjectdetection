
using System;
using System.Runtime.InteropServices;

namespace EZXRCoreExtensions.ObjectDetection
{
    public enum IASImageFormat
    {
        IASImageFormat_YUV420v = 0,
        IASImageFormat_YUV420f = 1,
        IASImageFormat_BGRA = 2,
        IASImageFormat_YUV420A = 3,
        IASImageFormat_GREY = 4,
        IASImageFormat_RGBA = 5
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct IASImageRes
    {
        public float width;
        public float height;
    }

    public enum IASImagePointerType
    {
        IASImagePointerType_RawData,
        IASImagePointerType_iOS_CVPixelRef
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct IASInputImage
    {
        public IntPtr pImg0;
        public IntPtr pImg1;
        public IASImagePointerType imgPtrType;
        public IASImageFormat imgFormat;
        public IASImageRes imgRes;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
        public float[] imgIntrinsics;
        public double timestamp;
    }
}