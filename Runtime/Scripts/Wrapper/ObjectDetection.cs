using System.Runtime.InteropServices;
using BaseDetector = System.IntPtr;
using BaseTracker = System.IntPtr;

namespace EZXRCoreExtensions.ObjectDetection
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ObjectDetTrackInfo
    {
        public float x1;
        public float y1;
        public float width;
        public float height;
        public int curr_id;
        public int label;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string class_name;
        public float prob;
    }
    
    public static partial class ExternApi
    {

#if (UNITY_IOS || UNITY_TVOS || UNITY_WEBGL) && !UNITY_EDITOR
        public const string LIBRARY_NAME = "__Internal";
#else
        public const string LIBRARY_NAME = "ObjectDetection";
#endif
        [DllImport(LIBRARY_NAME)]
        public static extern BaseDetector createDetector(string assetsDir);

        [DllImport(LIBRARY_NAME)]
        public static extern int detect(BaseDetector detector, IASInputImage inputImage, System.IntPtr objectDetTrackInfosPtr);

        [DllImport(LIBRARY_NAME)]
        public static extern void destroyDetector(BaseDetector detector);

        [DllImport(LIBRARY_NAME)]
        public static extern BaseTracker createTracker();

        [DllImport(LIBRARY_NAME)]
        public static extern int tracking(BaseTracker tracker, System.IntPtr objectDetTrackInfo, int numPerson);

        [DllImport(LIBRARY_NAME)]
        public static extern void destroyTracker(BaseTracker tracker);
    }
    
}

