using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

using BaseDetector = System.IntPtr;
using BaseTracker = System.IntPtr;

namespace EZXRCoreExtensions.ObjectDetection
{
    public class ObjectDetectionController
    {
        private string m_asset_path = "";
        private BaseDetector m_detector;
        private BaseTracker m_tracker; 

        private const int MAX_object_NUM = 10;

        private IntPtr m_objectDetTrackInfosPtr  = IntPtr.Zero;

        //析构函数
        ~ObjectDetectionController()
        {
            if (m_objectDetTrackInfosPtr != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(m_objectDetTrackInfosPtr);
                m_objectDetTrackInfosPtr = IntPtr.Zero;
            }
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="model_path">模型路径</param>
        /// <returns></returns>
        public int StartDecetion(string model_path)
        {
            if (m_detector != System.IntPtr.Zero)
            {
                Debug.LogError("detector is created");
                return 0;
            }
            m_asset_path = model_path;
            Debug.Log("model_path:" + model_path);
            m_detector = ExternApi.createDetector(model_path);
            if (m_detector == System.IntPtr.Zero)
            {
                Debug.LogError("detector ctrate failed");
                return -1;
            }
            m_objectDetTrackInfosPtr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(ObjectDetTrackInfo)) * MAX_object_NUM);
            return 1;
        }
        
        /// <summary>
        /// 释放
        /// </summary>
        public void StopDecetion()
        {
            if (m_detector != System.IntPtr.Zero)
            {
                ExternApi.destroyDetector(m_detector);
                m_detector = System.IntPtr.Zero;
            }
        }

        /// <summary>
        /// 开始追踪
        /// </summary>
        /// <returns></returns>
        public int StartTracking()
        {
            if (m_tracker != System.IntPtr.Zero)
            {
                Debug.LogError("tracker is created");
                return 0;
            }
            if (m_detector == System.IntPtr.Zero)
            {
                Debug.LogError("detector is null");
                return -1;
            }
            m_tracker = ExternApi.createTracker();
            if (m_tracker == System.IntPtr.Zero)
            {
                Debug.LogError("tracker ctrate failed");
                return -2;
            }
            return 1;
        }

        /// <summary>
        /// 停止追踪
        /// </summary>
        public void StopTracking()
        {
            if (m_tracker != System.IntPtr.Zero)
            {
                ExternApi.destroyTracker(m_tracker);
                m_tracker = System.IntPtr.Zero;
            }
        }

        /// <summary>
        /// 检测
        /// </summary>
        /// <param name="inputImage">输入图像</param>
        /// <returns></returns>
        public int RunDetect(IASInputImage inputImage)
        {
            if (m_detector == System.IntPtr.Zero)
            {
                Debug.LogError("detector is null");
                return 0;
            }
            int object_num = ExternApi.detect(m_detector, inputImage, m_objectDetTrackInfosPtr);
            return object_num;
        }

        /// <summary>
        /// 追踪
        /// </summary>
        /// <param name="object_num">检测到物体数量</param>
        /// <returns></returns>
        public List<ObjectDetTrackInfo> RunTrack(int object_num)
        {
            int object_num_track = 0;
            if (m_tracker != System.IntPtr.Zero)
            {
                object_num_track = ExternApi.tracking(m_tracker, m_objectDetTrackInfosPtr, object_num);
            }
            List<ObjectDetTrackInfo> objectList = new List<ObjectDetTrackInfo>();
            for (int i = 0; i < object_num_track; i++)
            {
                IntPtr elementPtr = m_objectDetTrackInfosPtr + i * Marshal.SizeOf<ObjectDetTrackInfo>();
                ObjectDetTrackInfo element = Marshal.PtrToStructure<ObjectDetTrackInfo>(elementPtr);
                objectList.Add(element);
            }
            return objectList;
        }
    }
}