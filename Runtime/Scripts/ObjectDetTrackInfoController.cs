using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace EZXRCoreExtensions.ObjectDetection
{
    public class ObjectDetTrackInfoComparer : IEqualityComparer<ObjectDetTrackInfo>
    {
        public bool Equals(ObjectDetTrackInfo x, ObjectDetTrackInfo y)
        {
            return x.curr_id == y.curr_id;
        }

        public int GetHashCode(ObjectDetTrackInfo obj)
        {
            return obj.curr_id.GetHashCode();
        }
    }

    public class ObjectDetTrackInfoController : MonoBehaviour
    {
        [NonSerialized]
        public Action<List<ObjectDetTrackInfo>> m_OnAddObjectDetTrackInfo = null;
        [NonSerialized]
        public Action<List<ObjectDetTrackInfo>> m_OnRemoveObjectDetTrackInfo = null;
        [NonSerialized]
        public Action<List<ObjectDetTrackInfo>> m_OnUpdateObjectDetTrackInfo = null;
        private List<ObjectDetTrackInfo> m_objectDetTrackInfos = new List<ObjectDetTrackInfo>();

        // Start is called before the first frame update
        void Start()
        {
            m_objectDetTrackInfos.Clear();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void UpdateObjectDetTrackInfo(List<ObjectDetTrackInfo> ObjectDetTrackInfos, int imageWidth, int imageHeight)
        {
            if (ObjectDetTrackInfos == null)
            {
                return;
            }
            List<ObjectDetTrackInfo> ObjectDetTrackInfos_new = new List<ObjectDetTrackInfo>();
            for (int i = 0; i < ObjectDetTrackInfos.Count; i++)
            {
                //for Portrait
                ObjectDetTrackInfo ObjectDetTrackInfo = GetObjectDetTrackInfo(ObjectDetTrackInfos[i], imageHeight, imageWidth);
                ObjectDetTrackInfos_new.Add(ObjectDetTrackInfo);
            }

            // Find the common elements between the two lists
            List<ObjectDetTrackInfo> commonElements = new List<ObjectDetTrackInfo>();
            foreach (ObjectDetTrackInfo element_new in ObjectDetTrackInfos_new)
            {
                foreach (ObjectDetTrackInfo element_old in m_objectDetTrackInfos)
                {
                    if (element_new.label == element_old.label)
                    {
                        commonElements.Add(element_new);
                        break;
                    }
                }
            }
            //Debug.Log("commonElements 1 = " + GetStr(m_objectDetTrackInfos));
            UpdateObjectDetTrackInfo(commonElements);
            //Debug.Log("commonElements 2 = " + GetStr(commonElements));
            // Find the elements in m_objectDetTrackInfos that are not in ObjectDetTrackInfos_new
            List<ObjectDetTrackInfo> reomveElements = m_objectDetTrackInfos.Except(ObjectDetTrackInfos_new, new ObjectDetTrackInfoComparer()).ToList();
            RemoveObjectDetTrackInfo(reomveElements);
            // Find the elements in ObjectDetTrackInfos_new that are not in m_objectDetTrackInfos
            List<ObjectDetTrackInfo> addElements = ObjectDetTrackInfos_new.Except(m_objectDetTrackInfos, new ObjectDetTrackInfoComparer()).ToList();
            AddObjectDetTrackInfo(addElements);
            m_objectDetTrackInfos = ObjectDetTrackInfos_new;
        }

        private void AddObjectDetTrackInfo(List<ObjectDetTrackInfo> ObjectDetTrackInfos)
        {
            if (ObjectDetTrackInfos != null && ObjectDetTrackInfos.Count > 0 && m_OnAddObjectDetTrackInfo != null)
            {
                m_OnAddObjectDetTrackInfo(ObjectDetTrackInfos);
            }
        }

        private void RemoveObjectDetTrackInfo(List<ObjectDetTrackInfo> ObjectDetTrackInfos)
        {
            if (ObjectDetTrackInfos != null && ObjectDetTrackInfos.Count > 0 && m_OnRemoveObjectDetTrackInfo != null)
            {
                m_OnRemoveObjectDetTrackInfo(ObjectDetTrackInfos);
            }
        }

        private void UpdateObjectDetTrackInfo(List<ObjectDetTrackInfo> ObjectDetTrackInfos)
        {
            if (ObjectDetTrackInfos != null && ObjectDetTrackInfos.Count > 0 && m_OnUpdateObjectDetTrackInfo != null)
            {
                m_OnUpdateObjectDetTrackInfo(ObjectDetTrackInfos);
            }
        }

        private ObjectDetTrackInfo GetObjectDetTrackInfo(ObjectDetTrackInfo info, int imageWidth, int imageHeight)
        {
            //目前返回时竖屏图像
            float scale_screen = (float)Screen.width / Screen.height;
            float scale_image = (float)imageWidth / imageHeight;
            float Object_x, Object_y, Object_width, Object_height;
            if (scale_screen > scale_image)
            {
                float scale = (float)Screen.width / imageWidth;
                float offset_y = (imageHeight * scale - Screen.height) / 2;
                Object_x = info.x1 * scale;
                Object_y = info.y1 * scale - offset_y;
                Object_width = info.width * scale;
                Object_height = info.height * scale;
            }
            else
            {
                float scale = (float)Screen.height / imageHeight;
                float offset_x = (imageWidth * scale - Screen.width) / 2;
                Object_x = info.x1 * scale - offset_x;
                Object_y = info.y1 * scale;
                Object_width = info.width * scale;
                Object_height = info.height * scale;
            }
           
            ObjectDetTrackInfo ObjectDetTrackInfo = new ObjectDetTrackInfo();
            ObjectDetTrackInfo.x1 = Object_x;
            ObjectDetTrackInfo.y1 = Object_y;
            ObjectDetTrackInfo.width = Object_width;
            ObjectDetTrackInfo.height = Object_height;
            ObjectDetTrackInfo.curr_id = info.curr_id;
            ObjectDetTrackInfo.label = info.label;
            ObjectDetTrackInfo.class_name = info.class_name;
            ObjectDetTrackInfo.prob = info.prob;
            return ObjectDetTrackInfo;
        }
        
        private string GetStr(List<ObjectDetTrackInfo> ObjectDetTrackInfos)
        {
            string str = "";
            foreach (ObjectDetTrackInfo ObjectDetTrackInfo in ObjectDetTrackInfos)
            {
                str += ObjectDetTrackInfo.label + " " + ObjectDetTrackInfo.x1 + " " + ObjectDetTrackInfo.y1 + " " + ObjectDetTrackInfo.width + " " + ObjectDetTrackInfo.height + "\n";
            }
            return str;
        }
    }
}